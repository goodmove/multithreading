#include "../utils/utils.h"

#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>

const int THREADS_COUNT = 4;
const int MAX_LINES_TO_PRINT = 10;
#define MAX_STRING_LENGTH 20

typedef struct ThreadPayload_s {
  size_t threadNumber;
  size_t linesCount;
  char stringData[MAX_STRING_LENGTH];
} ThreadPayload;



void PrintLines(const char* str, const int linesCount) {
  for (int i = 0; i < linesCount; i++) {
    printf("%s. Line: %d\n", str, i);
  }
}

void* Run(void* thread_payload) {
  ThreadPayload* payload = (ThreadPayload*)(thread_payload);
  PrintLines(payload->stringData, payload->linesCount);

  return NULL;
}

int InitThreadPayload(ThreadPayload* payload, size_t threadNumber) {
  if (payload == NULL) {
    return EINVAL;
  }

  static int isRandomSeeded = 0;
  if (!isRandomSeeded) {
    srand(time(NULL));
    isRandomSeeded = 1;
  }

  memset(payload->stringData, 0, sizeof(payload->stringData));
  sprintf(payload->stringData, "Thread #%03zu", threadNumber);
  payload->threadNumber = threadNumber;
  payload->linesCount = rand() % MAX_LINES_TO_PRINT;

  return SUCCESS;
}


void LogError(const char* const msg, const int errorCode) {
  fprintf(stderr, "%s. %s\n", msg, strerror(errorCode));
}

void LogErrorSystem(const char* const msg, const int errorCode) {
  fprintf(stderr, "%s: %s.\n", msg, strerror(errorCode));
}

ThreadPayload* AllocPayload() {
  return (ThreadPayload*)malloc(sizeof(ThreadPayload) * THREADS_COUNT);
}

void FreePayload(void* payload) {
  free(payload);
}

/**
  * Tries to stop ALL working threads by calling pthread_join() on them.
  * @args
  *   1) pthread_t* threads (non-null) - pointer to pthreads_t array
  *   2) int workingThreadsCount - number of workingThreads. Programmer must provide
  * correct number himself.
  * @return (int)
  *   1) SUCCESS on success
  *   2) EINVAL in case an argument passed is illegal
  *   3) error code returned by pthread_join() if a thread failed to join
  *   because deadlock occurred or thread is not joinable
*/
int StopWorkingThreads(pthread_t* threads, int workingThreadsCount) {
  if (threads == NULL || workingThreadsCount < 0) {
    return EINVAL;
  }

  for (int threadNumber = 0; threadNumber < workingThreadsCount; ++threadNumber) {
    int code = pthread_join(threads[threadNumber], NULL);
    if (code == EDEADLK || code == EINVAL) {
      return code;
    }
  }

  return SUCCESS;
}

/**
  * Tries to stop working threads. If stopping failed, error message is printed
  * for the corresponding error code and the program exits with EXIT_FAILURE.
  *
  * @params
  *  threads - array of pthread_t identifiers - threads to stop
  *  threadsToStop - number of threads to stop (function will try to stop threads
  * from 0 to threadsToStop)
  *  msg - message to print in the event of error
  */

void StopThreadsOrExitWithMessage(pthread_t* threads, int threadsToStop, void (*CleanUpFunction)(void*), void* payload) {
  int code = StopWorkingThreads(threads, threadsToStop);
  if (code != SUCCESS) {
    ExitWithCleanUp(EXIT_FAILURE, CleanUpFunction, payload);
  }
}


int main(int argc, char const *argv[]) {
  pthread_t threads[THREADS_COUNT];
  ThreadPayload* payload = AllocPayload();

  if (payload == NULL) {
    fprintf(stderr, "Failed to allocate memory for threads payload");
    exit(EXIT_FAILURE);
  }

  for (int threadNumber = 0; threadNumber < THREADS_COUNT; ++threadNumber) {
    int result = InitThreadPayload(payload + threadNumber, threadNumber);
    if (result != SUCCESS) {
      LogError("Failed to fill thread payload", result);
      StopThreadsOrExitWithMessage(threads, threadNumber, FreePayload, (void*)payload);
      ExitWithCleanUp(EXIT_FAILURE, FreePayload, (void*)payload);
    }

    result = pthread_create(threads + threadNumber, NULL, Run, payload + threadNumber);
    if (result != SUCCESS) {
      LogError("Failed to join thread", result);
      StopThreadsOrExitWithMessage(threads, threadNumber, FreePayload, (void*)payload);
      ExitWithCleanUp(EXIT_FAILURE, FreePayload, (void*)payload);
    }
  }

  int result = StopWorkingThreads(threads, THREADS_COUNT);
  if (result != SUCCESS) {
    ExitWithMessage("Failed to join thread", result);
  }

  FreePayload(payload);

  return EXIT_SUCCESS;
}
