#include "../utils/utils.h"
#include "./main.h"

#include <errno.h>

const int ITERATIONS = 10e6;
const int EXPECTED_ARGS_COUNT = 2;
const int THREADS_COUNT_INDEX = 1;
const int PRECISION = 15;



int main(int argc, char const *argv[]) {
  if (argc < EXPECTED_ARGS_COUNT) {
    PrintHelp();
    exit(EXIT_SUCCESS);
  }

  int threadsCount = atoi(argv[THREADS_COUNT_INDEX]);
  if (threadsCount <= 0) {
    ExitWithMixedMessage("Number of threads should be a positive integer", EINVAL, EXIT_FAILURE);
  }

  pthread_t threads[threadsCount];
  ThreadPayload* piCalculationData = AllocPayload(threadsCount);
  if (piCalculationData == NULL) {
    ExitWithCustomMessage("Failed to allocate threads payload", EXIT_FAILURE);
  }

  FillThreadsPayload(piCalculationData, threadsCount);
  StartPiCalculation(threads, threadsCount, piCalculationData);
  double pi = GatherPiValue(threads, threadsCount, piCalculationData);

  PrintDouble(pi, PRECISION);

  FreePayload(piCalculationData);

  return EXIT_SUCCESS;
}

/* ********************** Utility functions ********************** */

void PrintHelp() {
  printf("Usage: ./<program_name> threads_count\n");
}

void PrintDouble(const double d, const int precision) {
  printf("%.*lf\n", precision, d);
}

/* ********************** Pi calculation routines ********************** */

void* Run(void* arg) {
  ThreadPayload* info = (ThreadPayload*)arg;
  info->result = ApproxPiParallel(info->startIndex, info->iterations);
  return (void*) info;
}

double ApproxPiParallel(const int startIndex, const int iterations) {
  double approx = 0;
  for (int k = startIndex + 1; k < iterations; ++k) {
    double sign = (double)(k % 2);
    approx += (2*sign - 1) / (2*k-1);
  }
  return approx * 4;
}

void StartPiCalculation(pthread_t* threads, int threadsCount, ThreadPayload* piCalculationData) {
  for (int i = 0; i < threadsCount; ++i) {
    int code = pthread_create(threads + i, NULL, Run, piCalculationData + i);
    if (code != SUCCESS) {
      LogError("Failed to start new thread", code);
      ExitWithCleanUp(EXIT_FAILURE, FreePayload, piCalculationData);
    }
  }
}

double GatherPiValue(pthread_t* threads, int threadsCount, ThreadPayload* piCalculationData) {
  double PI = 0;
  for (int i = 0; i < threadsCount; ++i) {
    ThreadPayload* piInfo;
    int code = pthread_join(threads[i], (void*)&piInfo);
    if (code != SUCCESS) {
      LogError("PI calculation failed. Unable to join thread", code);
      ExitWithCleanUp(EXIT_FAILURE, FreePayload, piCalculationData);
    }
    PI += piInfo->result;
  }
  return PI;
}

/* ********************** Payload initialization routines ********************** */

ThreadPayload* AllocPayload(int threadsCount) {
  return (ThreadPayload*)malloc(sizeof(ThreadPayload) * threadsCount);
}

void FreePayload(void* payload) {
  free(payload);
}

void FillThreadsPayload(ThreadPayload* info, size_t threadsCount) {
  int itersPerThread = ITERATIONS / threadsCount;
  int rem = ITERATIONS % threadsCount;
  info[0].startIndex = 0;
  info[0].iterations = (itersPerThread) + (rem > 0);
  for (int i = 1; i < threadsCount; ++i) {
    info[i].startIndex = info[i-1].startIndex + info[i-1].iterations;
    info[i].iterations = info[i].startIndex + i * (itersPerThread) + (rem > i);
  }
}
