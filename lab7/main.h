#ifndef MAIN_H
#define  MAIN_H

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

typedef struct ThreadPayload_s {
  int startIndex;
  int iterations;
  double result;
} ThreadPayload;

/**
  * Function to approximate PI value in parallel mode via infinite series formula
  *
  * @args
  *   startIndex : int - the series term index to start with
  *   iterations : int - number of terms to include in calculation
  * @return
  *   double - sum of the corresponding part of approximation series
  */
double ApproxPiParallel(const int startIndex, const int iterations);

/**
  * Thread starting function
  */
void* Run(void* arg);

ThreadPayload* AllocPayload(int threadsCount);
void FreePayload(void* payload);

/**
  * Prepare some data to start parallel computation
  *
  * @args
  *   info : ThreadPayload* - data to fill
  *   threadsCount : int - number of threads among which calculation will be
  * distributed
  *
  */
void FillThreadsPayload(ThreadPayload* info, size_t threadsCount);

void StartPiCalculation(pthread_t* threads, int threadsCount, ThreadPayload* piCalculationData);

/**
  * Join threads and reduce all calculated PI parts to one value.
  *
  * @args
  *   threads : pthread_t* - threads to join and gather PI parts from
  *   threadsCount : int - number of threads to join
  *   piCalculationData : ThreadPayload* - data to free in the event of error
  *
  * @return
  *   double - final approximate value of PI
  */
double GatherPiValue(pthread_t* threads, int threadsCount, ThreadPayload* piCalculationData);

void PrintHelp();
void PrintDouble(const double d, const int precision);

#endif // MAIN_H
