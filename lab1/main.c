#include <pthread.h>
#include <string.h>
#include <stdio.h>

const int LINES_TO_PRINT = 10;

void PrintLines(const char* str, const int linesCount) {
  for (int i = 0; i < linesCount; i++) {
    printf("%s\n", str);
  }
}

void* Run(void* thread_name) {
  char* threadName = (char*)(thread_name);
  PrintLines(threadName, LINES_TO_PRINT);

  return NULL;
}

int main(int argc, char const *argv[]) {
  pthread_t thread;
  int result = pthread_create(&thread, NULL, Run, "My Thread");
  if (result != 0) {
    printf("%s\n", strerror(result));
    pthread_exit(NULL);
  }

  PrintLines("Main thread", LINES_TO_PRINT);

  pthread_exit(NULL);
}
