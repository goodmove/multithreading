#include "../utils/utils.h"
#include "./main.h"

#include <errno.h>
#include <signal.h>

const int MIN_ITERATIONS = 10e6;
const int EXPECTED_ARGS_COUNT = 2;
const int THREADS_COUNT_INDEX = 1;
const int PRECISION = 15;

enum CalculationState {
  Running,
  Stopped
};

/*
  Used to trigger thread's state
*/
int globalState = Running;

int main(int argc, char const *argv[]) {
  if (argc < EXPECTED_ARGS_COUNT) {
    PrintHelp();
    exit(EXIT_SUCCESS);
  }

  int threadsCount = atoi(argv[THREADS_COUNT_INDEX]);
  if (threadsCount <= 0) {
    ExitWithMixedMessage("Number of threads should be a positive integer", EINVAL, EXIT_FAILURE);
  }

  if (SetSigIntHandler() == SIG_ERR) {
    ExitWithCustomMessage("Failed to set signal handler", EXIT_FAILURE);
  }

  pthread_t threads[threadsCount];
  ThreadPayload* piCalculationData = AllocPayload(threadsCount);
  if (piCalculationData == NULL) {
    ExitWithCustomMessage("Failed to allocate threads payload", EXIT_FAILURE);
  }
  FillThreadsPayload(piCalculationData, threadsCount);
  StartPiCalculation(threads, threadsCount, piCalculationData);
  double pi = GatherPiValue(threads, threadsCount, piCalculationData);

  PrintDouble(pi, PRECISION);

  FreePayload(piCalculationData);

  return EXIT_SUCCESS;
}




/* ********************** Utility functions ********************** */

void PrintHelp() {
  printf("Usage: ./<program_name> threads_count\n");
}

void PrintDouble(const double d, const int precision) {
  printf("%.*lf\n", precision, d);
}

void* SetSigIntHandler() {
  return signal(SIGINT, SigIntHandler);
}

void SigIntHandler(int sigNumber) {
  if (sigNumber == SIGINT) {
    globalState = Stopped;
  }
}

/* ********************** Pi calculation routines ********************** */

double ApproxPiParallel(const int startIndex, const int stepSize) {
  double approx = 0;
  // globalState is a global variable

  // add `i` counter to increase precision and prevent existing of threads,
  // which dont't start calculation at all
  for (int k = startIndex, i = 0; globalState == Running || i < MIN_ITERATIONS; k += stepSize) {
    approx += 1.0 / (k*4.0 + 1.0);
    approx -= 1.0 / (k*4.0 + 3.0);
    if (i < MIN_ITERATIONS) {
      ++i;
    }
  }

  return approx * 4;
}

void* Run(void* arg) {
  ThreadPayload* info = (ThreadPayload*)arg;
  info->result = ApproxPiParallel(info->startIndex, info->stepSize);
  return info;
}

void StartPiCalculation(pthread_t* threads, int threadsCount, ThreadPayload* piCalculationData) {
  for (int i = 0; i < threadsCount; ++i) {
    int code = pthread_create(threads + i, NULL, Run, piCalculationData + i);
    if (code != SUCCESS) {
      LogError("Failed to start new thread", code);
      ExitWithCleanUp(EXIT_FAILURE, FreePayload, piCalculationData);
    }
  }
}

double GatherPiValue(pthread_t* threads, int threadsCount, ThreadPayload* piCalculationData) {
  double PI = 0;
  for (int i = 0; i < threadsCount; ++i) {
    ThreadPayload* piInfo;
    int code = pthread_join(threads[i], (void*)&piInfo);
    if (code != SUCCESS) {
      LogError("PI calculation failed. Unable to join thread", code);
      ExitWithCleanUp(EXIT_FAILURE, FreePayload, piCalculationData);
    }
    PI += piInfo->result;
  }
  return PI;
}

/* ********************** Payload initialization routines ********************** */

ThreadPayload* AllocPayload(int threadsCount) {
  return (ThreadPayload*)malloc(sizeof(ThreadPayload) * threadsCount);
}

void FreePayload(void* payload) {
  free(payload);
}

void FillThreadsPayload(ThreadPayload* info, size_t threadsCount) {
  for (int i = 0; i < threadsCount; ++i) {
    info[i].startIndex = i;
    info[i].stepSize = threadsCount;
  }
}
