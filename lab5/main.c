#include "../utils/utils.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

const int SHOULD_EXECUTE = 0;
const int SECONDS_TO_WAIT = 3;


void CancellationHandler(void* arg) {
  printf("Cancelling thread\n");
}

void EndlessPrint() {
  for(int i = 0; ; ++i) {
    pthread_testcancel();
    printf("%d\n", i);
    sleep(1);
  }
}

void* Run(void* arg) {
  pthread_cleanup_push(CancellationHandler, NULL);
  EndlessPrint();
  pthread_cleanup_pop(SHOULD_EXECUTE);

  return NULL;
}

int main(int argc, char const *argv[]) {
  pthread_t thread;

  int code = pthread_create(&thread, NULL, Run, NULL);
  ExitOnError(code);

  sleep(SECONDS_TO_WAIT);

  code = pthread_cancel(thread);
  ExitOnError(code);
  code = pthread_join(thread, NULL);
  ExitOnError(code);

  return EXIT_SUCCESS;
}
