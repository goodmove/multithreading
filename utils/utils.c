#include "utils.h"

const void* SYS_ONLY = NULL;
const void* NO_CLEANUP = NULL;

void ExitOnError(int code) {
  if (code != SUCCESS) {
    fprintf(stderr, "%s\n", strerror(code));
    exit(EXIT_FAILURE);
  }
}

void ExitWithMixedMessage(const char* msg, int errCode, int exitCode) {
  if (msg == SYS_ONLY) {
    fprintf(stderr, "%s\n", strerror(errCode));
  } else {
    fprintf(stderr, "%s. %s\n", strerror(errCode), msg);
  }
  exit(exitCode);
}

void ExitWithCustomMessage(const char* msg, int exitCode) {
  fprintf(stderr, "%s\n", msg);
  exit(exitCode);
}


void ExitWithCleanUp(int exitCode, void (*cleanUpFunction)(void*), void* data) {
  if (cleanUpFunction != NO_CLEANUP) {
    cleanUpFunction(data);
  }
    exit(exitCode);
}


void LogError(const char* const msg, const int errorCode) {
  fprintf(stderr, "%s. %s\n", msg, strerror(errorCode));
}
