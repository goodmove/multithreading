#ifndef UTILS_H
#define UTILS_H

#include "constants.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

extern const void* SYS_ONLY;
extern const void* USER_MSG_ONLY;
extern const void* NO_CLEANUP;

void ExitOnError(int code);
void ExitWithMixedMessage(const char* msg, int errCode, int exitCode);
void ExitWithCustomMessage(const char* msg, int exitCode);
void ExitWithCleanUp(int exitCode, void (*cleanUpFunction)(void*), void* data);
void LogError(const char* const msg, const int errorCode);

#endif
