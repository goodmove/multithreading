#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

const int SUCCESS = 0;

const int LINES_TO_PRINT = 10;

void PrintLines(const char* str, const int linesCount) {
  for (int i = 0; i < linesCount; i++) {
    printf("%s %d\n", str, i);
  }
}

void* Run(void* thread_name) {
  char* threadName = (char*)(thread_name);
  PrintLines(threadName, LINES_TO_PRINT);

  return NULL;
}

void ExitWithMessage(int resultCode) {
  fprintf(stderr, "%s\n", strerror(resultCode));
  exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[]) {
  pthread_t thread;
  int resultCode = pthread_create(&thread, NULL, Run, "My Thread");
  if (resultCode != SUCCESS) {
    ExitWithMessage(resultCode);
  }

  resultCode = pthread_join(thread, NULL);
  if (resultCode != SUCCESS) {
    ExitWithMessage(resultCode);
  }

  PrintLines("Main thread", LINES_TO_PRINT);

  return EXIT_SUCCESS;
}
