#include "../utils/utils.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define TRUE 1

const int SECONDS_TO_WAIT = 2;
const int STDOUT = 1;

void EndlessPrint(const char* str) {
  while (TRUE) {
    printf("%s\n", str);
    pthread_testcancel();
  }
}

void* Run(void* arg) {
  EndlessPrint("String\n");
  return NULL;
}

int main(int argc, char const *argv[]) {
  pthread_t thread;

  int code = pthread_create(&thread, NULL, Run, NULL);
  ExitOnError(code);

  sleep(SECONDS_TO_WAIT);

  code = pthread_cancel(thread);
  ExitOnError(code);
  code = pthread_join(thread, NULL);
  ExitOnError(code);

  return EXIT_SUCCESS;
}
